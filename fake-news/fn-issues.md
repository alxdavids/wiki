# Issues

## Fake News scraping

Using the sources highlighted on the [sources
page](https://wiki.socialcomplexity.eu/doku.php?id=fake-news:data-sources)
we should investigate the possibility of retrieving specific fake news
stories that we want to use in social media research. Where an API or
structured dataset exists, we should be able to either query or download
the dataset in ready-made form. If there is no such structure, we may be
able to scrape fake news stories from the websites manually.

In the table below, we provide the current status of work to retrieve
data from each of the sources.

| Source | Possible to scrape? | Retrieved  | Notes |
|:------|:---:|:---:|:---|
| [EU vs disinformation](https://euvsdisinfo.eu/disinformation-cases/) | Y | Y | Scraped manually via webpage, see code [here](https://gitlab.com/alxdavids/eu-disinformation-scraper) |
| [Social Science One](https://dataverse.harvard.edu/file.xhtml?persistentId=doi:10.7910/DVN/TDOAPG/DGSAMS&version=6.2) | Y | N | Exists as a structured dataset. According to this [blog post](https://socialscience.one/blog/unprecedented-facebook-urls-dataset-now-available-research-through-social-science-one) access may be possible via CrowdTangle, so it may be best to wait until we have access. |
| [Poynter](https://www.poynter.org/tag/fake-news/) | N? | N | Seems that the dataset includes FN stories that are not necessarily links. Moreover, there is no page structure that allows easily recovering information on URLs/other materials easily. If we move to a model that allows processing headlines, then there may be scope to scrape stories however. |
| [Hoaxy](https://hoaxy.osome.iu.edu/) | Y? | N | It looks like Hoaxy may provide an interface that allows us to either make online queries, or to slowly retrieve and store individual datasets. However, I'm still a little bit confused by what Hoaxy is mainly providing (since a large part of their service is focused on providing visualisations). **Still investigating**. |
