# wiki

Documents for the wiki written in markdown and converted to markdown.
Requires `pandoc` version 1.13 or greater.

## quickstart

- Create a new directory and put a markdown file into it.
- Install [pandoc](https://pandoc.org/).
- Then run `sh convert.sh`.
- See dokuwiki files in the `output/` directory.
