# Secure computation framework

## Data sets that need to be kept apart and secret

- Survey results
- Twitter data retrieved from survey participants

## Tasks that need to be performed using this data

- Training of ML classifiers
- Statistical aggregations

## Individuals running tasks

- Researchers
- SPAC-authorised individuals

## Data structures

- Survey (each participant):
  - A feature-set represented as a vector of values corresponding to the
    survey answers
- Twitter (each profile):
  - A feature-set represented as a vector including classifications made
    based on the structure of their profile, and of their associated
    tweets 
  - In particular, note that this vector cannot involve raw data (from
    tweets or profiles), because the computation will probably become
    too expensive

## Required mathematical operations

- General:
  - Combining of feature sets
  - Running of required functionality F on combined feature-sets and
    outputting some response to the researcher
- ML classification:
  - F will be a ML model training algorithm, but will depend on the type
    of model that is being trained
- Statistical aggregations:
  - F will be standard statistical operations (mean, standard deviation,
    more complicated stuff) that are computed on the feature-sets that
    are provided

## Available methodologies

### Third-party assisted (TTP)

  - Requires all data subjects to trust a single third-party to view and
    process their data
  - Data is sent to the trusted third party, that runs F and returns the
    result to whoever

![Trusted Third Party](:pipeline:ttp.jpg?nolink)

### Secure multi-party computation (MPC)

  - Datasets are referred to as participants that engage in a protocol
    to compute F
  - Function output is retrieved by one or both of the participants
  - Mechanisms come with security guarantees that show that all data in
    the exchange preserves its privacy, beyond what is leaked by the
    function output
  - This is the most common mechanism for replacing the need for a
    trusted third-party

![MPC](:pipeline:mpc.jpg?nolink)

### Fully homomorphic encryption (HE)

  - Involves encrypting feature-sets into (vectors of) ciphertexts
    apriori with a specific encryption scheme that allows performing
    mathematical operations on the underlying plaintext
  - Such schemes allow computing the functionality by operating on the
    ciphertexts alone, and so this can be done by a server that is
    oblivious to the actual data that is used
  - Encryption would take place on participant machines (using a
    participant-specific public key), and then be sent to the
    computation server afterwards
  - The computation server would compute the required representation of
    F on the participant-specified ciphertexts and return the resulting
    ciphertexts to the participants
  - The participants will decrypt the result and return it to the
    researcher

![HE](:pipeline:he.jpg?nolink)

### Functional encryption (FE)

  - A newer cryptographic methodology that is similar to functional
    encryption, except the computing server is given a decryption key
    that allows decrypting the result of a particular F
  - So the computing server is only given permission to learn the
    function output on decryption, rather than anything about the
    function inputs
  - This has the advantage that the computing server does not need to
    return the results of the computation for an additional decryption
    step

![FE](:pipeline:fe.jpg?nolink)

## Available tools

### Overview

| Library | Link | Supports | Cryptography |
| :------ | :--- | :------- | :----------- |
| [PySyft](#pysyft) | <https://github.com/OpenMined/PySyft> | Secure PyTorch/TensorFlow training/inference | MPC and HE |
| [tf-encrypted](#tf-encrypted) | <https://github.com/tf-encrypted/tf-encrypted> | Secure TensorFlow training/inference | MPC and HE |
| [JIFF](#jiff) | <https://github.com/multiparty/jiff> | JS library for secure computation of specific functions ([examples](https://github.com/multiparty/jiff/tree/master/demos)) | MPC |
| [CryptoNN](#cryptonn) | <https://github.com/iRxyzzz/nn-emd> | Neural network training | FE |
| [SecureNN](#securenn) | <https://github.com/snwagh/securenn-public> | Neural network training/inference for 3 participants | MPC |
| [SecureML](#secureml) | <https://eprint.iacr.org/2017/396.pdf> | General ML model training/inference for 2 participants | MPC |
| [TenSEAL](#tenseal) | <https://github.com/OpenMined/TenSEAL/> | Tensor manipulation on encrypted data | HE |
| [MP-SPDZ](#mp-spdz) | <https://github.com/data61/MP-SPDZ> | Python library for secure computation of specific functions | MPC |

### PySyft

- link: <https://github.com/OpenMined/PySyft>
- supports: Secure PyTorch/TensorFlow training/inference
- underlying cryptography: MPC and HE
- notes:
  - Open source and many contributors (however there seems to be some
    sort of company involved)
  - Allows using somewhat standard Python APIs for ML
  - May allow keeping datasets separate

### tf-encrypted

- link: <https://github.com/tf-encrypted/tf-encrypted>
- supports: Secure TensorFlow training/inference
- underlying cryptography: MPC and HE
- notes:
  - Similar to PySyft
  - Open source (again seems to be some sort of company involved)
  - Allows using somewhat standard Python APIs for ML
  - May allow keeping datasets separate

### JIFF

- link: <https://github.com/multiparty/jiff>
- supports: JS library for secure computation of specific functions ([examples](https://github.com/multiparty/jiff/tree/master/demos))
- underlying cryptography: MPC
- notes:
  - Previously used for calculating gender pay-gap analysis without
    revealing explicit salaries
    ([link](https://www.cs.bu.edu/fac/best/res/papers/compass18.pdf))
  - Allows statistical aggregations as shown in
    [examples](https://github.com/multiparty/jiff/tree/master/demos))
  - Would require implementation of more complicated ML model functions

### CryptoNN

- link: <https://github.com/iRxyzzz/nn-emd>
- supports: Neural network training
- underlying cryptography: functional encryption
- notes:
  - Python implementation of functional encryption for neural network
    training
  - May be possible to adapt FE usage to more appropriate statistical
    functions

### SecureNN

- link: <https://github.com/snwagh/securenn-public>
- supports: Neural network training/inference for 3 participants
- underlying cryptography: MPC
- notes:
  - Would require setting up a third participant for the SPAC use-case,
    which would be somewhat redundant

### SecureML

- link: <https://eprint.iacr.org/2017/396.pdf>
- supports: General ML model training/inference for 2 participants
- underlying cryptography: MPC
- notes:
  - Unfortunately no public implementation exists
  - Would potentially be the best candidate if there were an
    implementation

### TenSEAL

- link: <https://github.com/OpenMined/TenSEAL/>
- supports: Tensor manipulation on encrypted data
- underlying cryptography: HE
- notes:
  - same company as PySyft
  - allows mathematical tensor operations using the SEAL HE library
  - may be the easiest method for building a workflow around homomorphic
    encryption
  - there appears to be a similar version offered by tf-encrypted:
    https://github.com/tf-encrypted/tf-seal

### MP-SPDZ

- link: <https://github.com/data61/MP-SPDZ>
- supports: Python library for secure computation of specific functions
- underlying cryptography: MPC
- notes:
  - similar situation to JIFF
  - however, there are
    [examples](https://github.com/data61/MP-SPDZ#tensorflow-inference)
    for using TensorFlow inference already

## Recommendations

For ease of use regarding the cryptographic side of things, I think that
either [JIFF](https://github.com/multiparty/jiff),
[PySyft](https://github.com/OpenMined/PySyft) or
[tf-encrypted](https://github.com/tf-encrypted/tf-encrypted) may be the
best candidates. All of these have documentation and tutorials available
that make it easier to integrate with existing programs. While PySyft
and tf-encrypted have the added bonus that they are written in Python
and integrate well with existing ML libraries such as PyTorch and
TensorFlow, JIFF has the advantage that it has been used in real-world
applications.

JIFF has the disadvantage that it is written in JavaScript, which may
mean that it comes with more difficulty for implementing complex ML
classifiers. JIFF essentially allows computing individual mathematical
operations and statistical aggregations. This means that one would have
to implement an ML model from scratch.

An alternative to JIFF would be to use
[MP-SPDZ](https://github.com/data61/MP-SPDZ), which is written in Python
and allows easier integration with typical machine learning libraries.
However, I found the documentation for MP-SPDZ much more confusing than
JIFF.

All of these techniques above use MPC-based mechanisms for performing
operations securely. I believe that tf-encrypted and PySyft may support
HE-based techniques also, but it's not clear that there is any real
benefit.
