# Pipeline Team <!-- omit in toc -->

## Overview

The pipeline team provides the infrastructure for collecting, storing
and analysing data for the purposes of research goals in the FARE
project.

There are separate pages for the list of [completed and available
tasks](https://wiki.socialcomplexity.eu/doku.php?id=pipeline:tasks), and
a list of [open questions and
issues](https://wiki.socialcomplexity.eu/doku.php?id=pipeline:issues).

There is also a page containing information regarding the computation of
functions on [separate datasets that should remain
private](https://wiki.socialcomplexity.eu/doku.php?id=pipeline:secure-computation-summary).

### Team members

- [Paulo](palmeida@lip.pt)
- [José](jreis@lip.pt)
- [Pedro](pedrorio@lip.pt)
- [Alex](adavidson@lip.pt)

### Code

The Pipeline team has a [sub-group in
GitLab](https://gitlab.com/socialcomplexity/pipeline) containing the
code repositories. The issue tracker for each individual project in
GitLab also contains an up-to-date list of the current tasks.

### Requests and issues

Any requests for new functionality/requirements of the pipeline can be
submitted by adding a [new
issue](https://wiki.socialcomplexity.eu/doku.php?id=pipeline:issues) to
the existing list table.

### Meetings

The pipeline team meets regularly every ... weeks on ... ?

**(Alex TODO: When do we meet?)**

## Components & requirements

We describe each of the components of the pipeline and how researchers
interact with it to retrieve data that is pulled from social media
platforms.

### System diagram

![diagram](:pipeline:diagram.png?nolink)

### Social media data

The main requirements for the functionality provided by the
pipeline team are listed below, refer to the diagram for an overview.

- Design (potentially generic) database (DB) schemas for each social
  media platform that will be the subject of research questions. The DB
  schemas will correspond to relational DBs that capture the information
  that is deemed necessary by the requirements of the [social
  media](https://wiki.socialcomplexity.eu/doku.php?id=socialmedia:intro)
  team.
- Create a mechanism that queries each platform API based on a set of
  predefined queries. The results of each query will be parsed and
  stored in DB tables corresponding to the schema designed previously.
- Provide an SQL interface for researchers to query the DB tables that
  are stored.
  
#### API queries

At regular time intervals (daily?) the pipeline each social media
platform with the available set of queries. Each query will specify a
set of parameters (such as applicable dates, keywords, users) and which
platform(s) it corresponds to. The set of currently used queries will be
modifiable by SPAC researchers via a separate interface (GitLab?). This
will allow adding and changing the queries that the pipeline is
currently making.

#### DB schema

The DB schema specifies the tables and their structures (e.g. columns,
indexing). The following tables will be available for each platform.

- `Users` (one user to each row; including relevant profile information)
- `Posts` (one post to each row; including user_id, text, URLs, dates,
  interaction statistics)

Depending on the platform, the following tables may also be created.

- `Network` (referencing a user in the `Users` table; lists user IDs
  that the user follows/is friends with).
- `Spread` (referencing a post in the `Posts` table; including
  users that interacted with the post, text, dates): primarily to
  monitor the network of a post.
- `ExpandedUsers` (referencing a single user in the `Users` table;
  including additional relevant data, such as 'likes' and other
  interactions).

#### DB storage

When queries are made, the responses will be categorised and then stored
in tables corresponding to the above schema.

#### DB query interface

To allow researchers to view the stored data, the pipeline team will
create an additional interface that allows retrieving records from each
of the tables. This interface will provide a pre-determined list of
queries that researchers can run to retrieve records from the DB.

The need for this interface is to prevent users from accessing the raw
data themselves. This is to prevent users from accidentally running SQL
queries that modify/delete the raw data, and/or to mitigate privacy
concerns associated with viewing such data.

### Survey data

A new DB will be created to store data associated with any FARE project
research. The schema of the database will correspond to the questions
and answers within the survey. If there are multiple surveys, then each
one will have its own DB/table assigned.

#### Retrieving survey data

Survey data will be retrievable via the same DB query interface that is
used for social media data.

### Privacy considerations

Privacy considerations associated with the FARE project are considered
as part of WP5.

#### Survey participant data

All data associated with the survey participants must be safeguarded to
ensure that personal data is not divulged beyond the necessary research
output. In addition, part of WP5 requires the usage of
privacy-preserving data processing and aggregation procedures that
enables researchers to perform statistical analysis on the data without
having to view the raw data. Such processes include:

- Data processing and storage policies that comply with existing
  legislation (such as the EU GDPR).
- Access control policies for the servers where survey data is held.
- Cryptographic tools such as secure multi-party computation to be used
  to perform statistical analysis and aggregation over joint data sets
  containing both survey and social media data. This prevents anyone
  with knowledge of the survey data being able to link it to the
  person's social media profile (and vice-versa).

#### General social media data

Data received from social media APIs corresponds to public profiles,
though usage of such data must comply with the usage policy of each
platform. This may include deleting records from DBs when such records
are deleted from the platforms.

While such data is public, it would be advantageous to work with it in
such a way that researchers are not exposed to individual-level data.
Such mechanisms may include using privacy-preserving tools such as
differential privacy or encrypted computation to allow publishing of
summary statistics that are still useful for research purposes.

For now, such issues are considered as open questions and will be
discussed in due course.

### Legal considerations

All data that is retrieved and stored by the pipeline will be subject to
EU legislation around data protection (such as the GDPR), as well as
legal policies imposed by the data providers (such as the social media
platforms). This includes the following considerations.

- Data stored on individuals may be subject to removal if individuals
  wish to have their data removed.
- Platforms may request for certain records to be removed if they are
  deleted from the platforms themselves.((This may not apply in the case
  that data is sufficiently anonymised, but we would have to justify
  this carefully.))

**(Alex: Someone with more knowledge on these subjects may want to add
more conditions here).**
