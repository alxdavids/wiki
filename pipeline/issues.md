# Issues and open questions

This page collects high-level issues and open questions surrounding the
pipeline. The issues are summarised in the table below, any new issues
can also be added here. Individual tasks that are associated with
reaching a resolution of each the issues are tracked on the [tasks
page](https://wiki.socialcomplexity.eu/doku.php?id=pipeline:tasks).

The following sections detail the [open issues](#issue-tracker). Issues should be marked as
"Unsolved" while they continue to be discussed. Once a set of tasks is
drawn up to solve it it should be marked as "In progress". Once the
issue is resolved it will be marked as "Resolved". Detailed descriptions of each
of the issues should be added to the [Issue
Descriptions](#issue-descriptions) section.

## Issue tracker

| Issue | Related area | Priority | Status | Description | Owner |
| :--------------------------------------------------------------------------------------------------------------------------------------------------- | :----------: | :------: | :-------------: | :-----------------------------------------------------------------------: | :-----: |
| Twitter\'s [Developer Agreement and Policy](https://developer.twitter.com/en/developer-terms/agreement-and-policy) demands removal of deleted tweets |    Legal     | Moderate |  **Unsolved**   |                    [link](#removal-of-deleted-tweets)                     | -     |
| GDPR\'s right to be forgotten has to work with backups                                                                                               |    Legal     | Moderate |  **Unsolved**   |                             [link](#backups)                              | -     |
| Non-learning of Twitter handles                                                                                                                      |   Privacy    |   Low    | **In research** | [link](#prevent-us-from-learning-twitter-handles-for-survey-participants) | Alex  |
| Information retrieval portal                                                                                                                         |    Legal     |   Low    | **In research** |               [link](#private-information-retrieval-portal)               | -     |
| Secure computation implementation                                                                                                                    |   Privacy    | Moderate |  **Unsolved**   |                [link](#secure-computation-implementation)                 | Alex  |
| Using differential privacy for protecting datasets | Privacy | Low | **In research** | [link](#differential-privacy) | Alex | 

### Statuses

- **Done**: Indicates that all work has been completed.
- **In progress**: Work is in progress, project is being actively developed, or in review.
- **Unsolved**: Problem must be solved, but work has not been started yet.
- **In research**: It is unclear whether this work is required yet. Further research & discussions still required.

## Issue descriptions

The descriptions below are referenced by a row in the tables above.

### Removal of deleted tweets

According to Twitter\'s [Developer Agreement and
Policy](https://developer.twitter.com/en/developer-terms/agreement-and-policy):

> If Twitter Content is deleted, gains protected status, or is otherwise
> suspended, withheld, modified,
>
> or removed from the Twitter Applications (including removal of
> location information), you will make all
>
> reasonable efforts to delete or modify such Twitter Content (as
> applicable) as soon as possible, and in
>
> any case within 24 hours after a written request to do so by Twitter
> or by a Twitter user with regard
>
> to their Twitter Content, unless prohibited by applicable law or
> regulation and with the express
>
> written permission of Twitter.

This is hard to do, technically, and the research community seems to be
uncertain about the legality and ethics, as written in [a blog post by
Justin
Littman](https://medium.com/on-archivy/twitters-developer-policies-for-researchers-archivists-and-librarians-63e9ba0433b2).
The issue had been discussed in [a paper by Maddock et
al.](https://faculty.washington.edu/kstarbi/maddock_starbird_tweet_deletions.pdf),
which called for open discussion saying that \"the current strategy ---
either plausible deniability or blissful ignorance --- should be
addressed\". This issue might be particularly serious in our case since
we are working with fake-news related tweets. Deletions here could occur
due to twitter directly deleting them ([see for example twitter\'s
election integrity
policy](https://help.twitter.com/en/rules-and-policies/election-integrity-policy))
or in the case of retweets, which get deleted automatically when the
source tweets were deleted or when the source accounts were removed
([see here](https://help.twitter.com/en/using-twitter/retweet-faqsm)).

Possible strategies to remove deleted tweets are to subscribe to a
stream that broadcasts deletions and act on them (but we may miss some)
or to systematically and continuously check the status of tweets in our
database and delete those that are not found on the Twitter server (but
this will hit the API\'s rate limit and take an increasingly long time
as the database grows).

This issue is postponed for now, as it is not central to the design of
the pipeline. Whatever solution we eventually use (if any) can be built
on top of the database.

### Backups

Due to GDPR\'s \"right to be forgotten\", we can\'t rely on traditional
backups where data are stored by date and not usually edited or
selectively deleted. This is a known issue that we need to discuss and
then look for current solutions.

- [Backups and the right to be forgotten in the GDPR: An uneasy
  relationship](https://www.sciencedirect.com/science/article/abs/pii/S0267364918301389)
- [Data Management vs. GDPR: Solving the Right to Be Forgotten
  Problem](https://www.aparavi.com/data-management-gdpr-right-to-be-forgotten/)

Given the scientific nature of our study, we _may_ be exempted from the
right to be forgotten to the extent that the following exemption is
made.((The exemption can be found in article 17/3/d) interpreted
in conjunction with article 89 of the GDPR))

> it is likely to render impossible or seriously impair the achievement
> of the objectives of that processing.

This does not impact the ethical side of it and it should be noted that
the:

> likely to impair ... objectives of processing

is broad. Furthermore, it is an important problem so any
proof-of-concept solution would be scientifically relevant.\

### Prevent us from learning Twitter handles for survey participants

It would be interesting to see if we could prevent ourselves from
learning Twitter handles for those that sign up for any survey that we
use. Any personal identifiers would be replaced with cryptographic
hashes, or similar. This would essentially require the user registering
to provide us with their Twitter data after accessing the API
themselves. This work is **not** required in the sense that FARE has
already gained approval for retrieving such data directly. Doing this
would likely provide users with a higher degree of confidence that we
are treating their data carefully. However, any such solution would have
to be implemented in such a way that it would still allow us
subsequently to perform meaningful data analysis.

Possible options include:

- Set up a proxy web server with an in-built Twitter API key that users
  are redirected to query the Twitter API for all of their tweets. Users
  would only have to provide their Twitter handle. All data would be
  stored in a local database by the web server. The web server would run
  in our available servers, but researchers would never access the local
  databases themselves.
- Set up a web server as above, but that runs in a different computing
  location potentially overseen by known collaborators. This would
  provide greater disconnection between data and researchers, but would
  probably be more difficult to maintain.
- Provide users with an API key for querying themselves and then asking
  them to provide their data directly to us. This is **unlikely** to be
  viable as providing users with such an API key could lead to abuse.

In the cases above that involve setting up web servers, future data
analysis would only be performed via cryptographic instruments that
ensure that the local database is kept secret, even after computing
supported aggregations. All other access by researchers would be
prohibited. Such a system would almost definitely need to
use implementations of [secure computation
tools](#secure-computation-implementation) to allow performing
statistical analysis. There are additional discussions around the types
of allowed queries, and whether additional measures such as differential
privacy may be required.

Prohibiting access to the data could either be enforced via stringent
access control measures (specified in the operating system layer), or by
encrypting the database. It would probably be preferential to go for the
access control approach. Encrypting the DB would further inhibit
statistical analysis from efficiency and usability perspectives.
Moreover, learning the end results would still require access to the
decryption key, transitively requiring that this key has limited
exposure to researchers via standard access control measures. Thus,
leading to a sort of circular security assumption.

### Information retrieval portal

An idea that Paulo and Alex discussed recently was whether it would be
possible for users to retrieve their social media data from us. This may
potentially allow users to selectively delete their data without having
to ask us to do this for them. Such an approach would be worthwhile, and
may have wider use in research projects that use similar data.

Not much thought has been put into this yet, but the following
considerations would have to be made:

- Any such tools that we designed would have to be very carefully
  designed to ensure that data could not be retrieved by anyone who did
  not own it. While social media data is usually public, it would
  probably be worthwhile for us to ensure that only the real individual
  could retrieve their own data. This could be done by forcing the user
  to connect to their social media account using a protocol like OAuth
  so that we can check their credentials before allowing any query.
- Again, it would be advantageous to allow users to retrieve their data,
  without us learning their identity. This may be difficult if we need
  them to authenticate to the service provider over OAuth, as we will
  learn their social media identity. Private Information Retrieval (PIR)
  is quite a hot topic in cryptography, so there should be tools that
  allow this. But, there are further questions.
  - Are they practical to run efficiently?
  - Would they be easy to set up?
  - How would we check that the user was indeed the person that they say
    they are?

### Secure computation implementation

Secure computation tools (sometimes shortened to MPC for multi-party
computation) ensure that functions can be collaboratively evaluated over
disparate datasets, without compromising the privacy of said datasets
(beyond what the function output reveals, anyway). It will be necessary
to use secure computation when statistically comparing and analysing
data taken from social media data alongside sources of private data
provided directly by users (via surveys and other methods).

MPC algorithms vary in efficiency and security requirements. There many
underlying cryptographic methods based around different mathematical
tools. Moreover, protocols can either be *semi-honest* or *maliciously*
secure. Semi-honest adversaries are expected to play by the
protocol rules, but will try and learn things only from the flow of
messages themselves. Malicious adversaries may try any arbitrary
cheating strategy. For this work, I think we can assume that we only
need *semi-honest* security, since the protocols will be run on our
architecture, and it's unlikely that these processes will be infiltrated
by malicious actors.

Fortunately, many tools exist for performing secure computation on
datasets, though they are quite early in their development. This means
that they are not usually part of integrated systems or libraries that
can be used very easily. In terms of available suites for doing this, my
research has lead me to the following projects:

- [JIFF](https://github.com/multiparty/jiff): An MPC framework built in
  JavaScript that allows for many general computations based on a
  singular set of cryptographic algorithms. Current examples
  [here](https://github.com/multiparty/jiff/tree/master/demos). This
  software has been used by organisations for comparing salaries related
  to producing gender pay gap statistics. Provides _semi-honest_ security
  guarantees. Actively developed by a team at [Boston
  University](https://multiparty.org/).
- [MP-SPDZ](https://github.com/data61/MP-SPDZ): A multi-purpose MPC
  framework allowing computation using many different underlying
  cryptographic methods. Supports both semi-honest and maliciously
  secure variants. Supports many different
  [functions](https://github.com/data61/MP-SPDZ/tree/master/Programs/Source)
  written in Python-like pseudocode. The project has been used quite
  widely in developing research in training machine learning models.
  Very actively developed by Marcel Keller, a well-known cryptography
  researcher. Remains to be seen whether it would be easy to integrate
  into a wider system, but python interface shouldn't be too hard to
  use.
- [SCALE-MAMBA](https://github.com/KULeuven-COSIC/SCALE-MAMBA): Very
  similar to MP-SPDZ (initially forked from the same source). Comes with
  a lot of
  [documentation](https://homes.esat.kuleuven.be/~nsmart/SCALE/) for
  running it, as well as similar sets of examples. Functions can either
  be written in the same language as MP-SPDZ, or in rust. The project
  seems to be slightly more stable in terms of development than MP-SPDZ.
  The toolkit has also been used widely in cryptographic research.
- [CrypTFlow](https://github.com/mpc-msri/EzPC): An end-to-end system
  for secure TensorFlow inference developed by Microsoft. Focused
  specifically on the machine learning use-case. Functionality appears
  to be written in a combination of python and C++. Seems actively
  developed in both research and in code. Documentation seems a little
  bit scarce, and it is unclear how easy it would be to build this into
  a wider system.
- [TF-encrypted](https://github.com/tf-encrypted/tf-encrypted): Allows
  building ML training & inference models privately, integrated with
  TensorFlow. Uses techniques found in both MPC and FHE literature.
  Links to a company known as [Cape Privacy](https://tf-encrypted.io/).
- [PySyft](https://github.com/OpenMined/PySyft): Similar goal and
  capabilities to TF-encrypted, though integrated with PyTorch.
  Developed by a company known as
  [OpenMined](https://www.openmined.org/).

A lot of my research has come about thanks to the work done
[here](https://github.com/mpc-sok/frameworks), comparing multiple
libraries for a SoK paper in 2019.

In terms of preferences, I think **JIFF** may be the most robust system to
use thanks to its previous use in existing research projects, and due to
it being written in a fairly simple programming language. That being
said, the range of functionality provided by JIFF is relatively small,
i.e. simple statistical analyses are possible (linear regressions,
calculations of standard deviations), but complicated machine learning
models are not currently implemented.

If we want to use more complicated functionality, it may be best to
investigate use of **TF-encrypted**, **PySyft** (for training) and
**CrypTFlow** (for inference). These libraries has stronger links with
the widely-used TensorFlow library for performing machine learning
functionality. This may allow easier implementation for those with prior
experience using this software, though this has not yet been verified.


MP-SPDZ provides some level of support for TensorFlow inference, so this
may be an alternative. However, the process, as documented, seems quite
involved and I have yet to try it out.

### Differential Privacy

Differential privacy (DP) provides guarantees that individuals in datasets
do not affect the results of queries, and thus their data remains
private. It works by adding noise to query results that obscures the
real values of queries, in balance with maintaining a certain resolution
of the real distribution of data, to ensure that data analysis is
possible without sacrificing privacy.

Using differential privacy comes with caveats, in that queries are
allowed up to a specified bound known as the *privacy budget*. Setting a
fixed privacy budget ensures that the privacy of the data is maintained.
Using up more than the budget allows could lead to data inference by
being able to subtract away averaged noise used in queries.

This poses a lot of questions around how to use differential privacy in
a manner that is still usable for scientific research. Moreover, for
this project, we would need to determine which datasets are required to
institute such guarantees. In particular, in theory, it would be great
if we could use differential privacy even for datasets that contain
ostensibly public data (such as social media data). However, this has to
be balanced against providing usable interfaces for performing research
and data analysis.

Libraries exist for performing statistical queries on differentially
private databases (see
[SmartNoise](https://github.com/opendifferentialprivacy/) by Microsoft
and OpenDP, and an offering by
[Google](https://github.com/google/differential-privacy)). The real
question is how we'd built it into a systematic pipeline. Such
techniques can also be combined with MPC approaches to further protect
MPC function outputs from inference attacks.

One possible approach would be to trial DP approaches by building a
query interface that allowed each researcher to have a query budget of
their own, for any given dataset. Moreover, when a data is collected, it
may be possible to siphon some small percentage (e.g. 10%) of the data
off as "training data" that does not use DP. When researchers are
trialling analysis, they run all queries on the training data. When they
have an idea of queries they want to run for research publications, they
then run the queries on the full DP data. This would ensure that the
query budget is only used for final queries, rather than in initial
research investigation. 

This would probably require a web interface for correctly interacting
with datasets. Moreover, it should be noted that using DP severely
limits what information can be revealed alongside any research
publication, for privacy reasons.

